Package.describe({
  name: 'mirrorcell:google-auth',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Cordova.depends({
  'cordova-plugin-googleplus': "https://github.com/EddyVerbruggen/cordova-plugin-googleplus.git#967e729947aeb754182f2f40ea474259f8e4fb0d"
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');

  api.use(['ecmascript',
    'meteor-base',
    'accounts-base',
    'check',
    "http"
  ]);

  api.addFiles([
    'lib/g_plus_client.js',
    'lib/styles.css'
  ], 'client');

  api.imply(['check', 'accounts-base']);

  api.addFiles('lib/g_plus_native_logout.js', 'web.cordova');
  api.addFiles(['lib/g_plus_server.js', 'lib/config.accounts.js'], 'server');

  api.addFiles([,
    'lib/g_plus_shared.js'
  ], ['client', 'server']);


});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('mirrorcell:google-auth');
  api.addFiles('google-auth-tests.js');
});
