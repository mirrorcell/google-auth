if(!(Meteor.settings.google.clientId && Meteor.settings.google.clientSecret )) {
    throw new Meteor.Error("Required-Values", "Google Client ID or Client Secret Were not Found");
}

var google = {
    clientId: Meteor.settings.google.clientId,
    clientSecret: Meteor.settings.google.clientSecret
};


Accounts.registerLoginHandler(function(req) { // cordova_g_plus SignIn handler
    if (!req.cordova_g_plus)
        return undefined;

    check(req, {
        cordova_g_plus: Boolean,
        email: String,
        oAuthToken: String,
        profile: Match.Any,
        sub: String
    });

    var user = Meteor.users.findOne({
          "services.google.id": req.sub
      }),
      userId = null,
      toks = null,
      info = null;

    var config = Accounts.loginServiceConfiguration.findOne({service: "google"});
    if (! config)
        throw new Meteor.Error(500, "Google service not configured.");

    if (!user) {
        toks = Meteor.http.call("POST",
          "https://www.googleapis.com/oauth2/v4/token",
          {
              params: {
                  'client_id': config.clientId,
                  'client_secret': config.secret,
                  'code': req.oAuthToken,
                  'grant_type': 'authorization_code'
              }
          });

        if(!toks) {
            throw new Meteor.Error(500, "Google Did not return a result");
        }

        info = Meteor.http.get("https://www.googleapis.com/oauth2/v3/userinfo", {
            headers: {
                "User-Agent": "Meteor/1.0"
            },

            params: {
                access_token: toks.data.access_token
            }
        });

        if (info.error) throw info.error;
        else if(toks.error) throw toks.error;
        else {
            if (req.sub == info.data.sub) {
                var googleResponse = _.pick(info.data, "email", "email_verified", "family_name", "gender", "given_name", "locale", "name", "picture", "profile", "sub");

                googleResponse["accessToken"] = toks.data.access_token;
                googleResponse["refreshToken"] = toks.data.refresh_token;
                googleResponse["id"] = req.sub;

                if (typeof(googleResponse["email"]) == "undefined") {
                    googleResponse["email"] = req.email;
                }

                var insertObject = {
                    createdAt: new Date(),
                    // profile: googleResponse,
                    services: {
                        google: googleResponse
                    }
                };

                if (req.profile && (req.profile instanceof Array)) { // fill profile according to req.profile
                    if (0 < req.profile.length) {
                        insertObject.profile = {};

                        for (var A = 0; A < req.profile.length; A++) {
                            if (_.has(googleResponse, req.profile[A])) {
                                insertObject.profile[req.profile[A]] = googleResponse[req.profile[A]];
                            }
                        }
                    }
                }

                userId = Meteor.users.insert(insertObject);
            } else throw new Meteor.Error(422, "AccessToken MISMATCH in hedcet:cordova-google-plus-native-sign-in package");
        }
    } else userId = user._id;

    var stampedToken = Accounts._generateStampedLoginToken();
    var stampedTokenHash = Accounts._hashStampedToken(stampedToken);

    Meteor.users.update({
        _id: userId
    }, {
        $push: {
            "services.resume.loginTokens": stampedTokenHash
        }
    });

    return {
        token: stampedToken.token,
        userId: userId
    };
});



var Future = Npm.require('fibers/future');

Meteor.methods({
    //Links a google plus account to an account without a existing google plus
    linkGPlus: function(req) {
        console.log("LINKING", req, google);
        var future = new Future(),
          user = null,
          toks = null;

        if (!req.cordova_g_plus)
            return undefined;

        check(req, {
            cordova_g_plus: Boolean,
            email: String,
            oAuthToken: String,
            profile: Match.Any,
            sub: String
        });

        if(!this.userId) {
            return
        } else {
            user = Meteor.users.findOne(this.userId);
        }

        var config = Accounts.loginServiceConfiguration.findOne({service: "google"});
        if (! config)
            throw new Meteor.Error(500, "Google service not configured.");

        if (user) {

            toks = Meteor.http.call("POST",
              "https://www.googleapis.com/oauth2/v4/token",
              {
                  params: {
                      'client_id': config.clientId,
                      'client_secret': config.secret,
                      'code': req.oAuthToken,
                      'grant_type': 'authorization_code'
                  }
              });

            if(!toks) {
                throw new Meteor.Error(404, "Google Did not return a result");
            }

            var res = Meteor.http.get("https://www.googleapis.com/oauth2/v3/userinfo", {
                headers: {
                    "User-Agent": "Meteor/1.0"
                },

                params: {
                    access_token: toks.data.access_token
                }
            });

            if (res.error) throw res.error;
            else if(toks.error) throw toks.error;
            else {
                if (req.sub == res.data.sub) {
                    var googleResponse = _.pick(res.data, "email", "email_verified", "family_name", "gender", "given_name", "locale", "name", "picture", "profile", "sub");

                    googleResponse["accessToken"] = toks.data.access_token;
                    googleResponse["refreshToken"] = toks.data.refresh_token;
                    googleResponse["id"] = req.sub;

                    if (typeof(googleResponse["email"]) == "undefined") {
                        googleResponse["email"] = req.email;
                    }

                    var updateObject = {
                        services: {
                            google: googleResponse
                        }
                    };

                    Meteor.users.update(this.userId, {
                        $set : {
                            'services.google' : updateObject.services.google
                        }
                    }, function(err, docs) {
                        console.log("ERROR", err, "DOCS", docs);
                        future.return(err, docs);
                    })

                } else throw new Meteor.Error(422, "AccessToken MISMATCH  package");
            }
        } else {
            future.return("ERROR");
        }

        return future.wait();
    },
    getRefreshToken : function(code) {
        var config = Accounts.loginServiceConfiguration.findOne({service: "google"});
        if (! config)
            throw new Meteor.Error(500, "Google service not configured.");

        console.log("Trying to refresh", code);
        var result;

        try {
            result = Meteor.http.call("POST",
              "https://www.googleapis.com/oauth2/v4/token",
              {
                  params: {
                      'client_id': config.clientId,
                      'client_secret': config.secret,
                      'code': code,
                      'grant_type': 'authorization_code'
                  }
              });

            if(result) {
                return result.data;
            } else {
                throw new Meteor.Error(500, "Google Did not return a result");
            }
        } catch (e) {
            var code = e.response ? e.response.statusCode : 500;
            console.log(e);
            throw new Meteor.Error(code, 'Unable to exchange google refresh token.', e);
        }
    },
    // Obtain a new access token using the refresh token
    exchangeGoogleToken: function(userId) {
        this.unblock();

        var user;
        if (userId && Meteor.isServer) {
            user = Meteor.users.findOne({_id: userId});
        } else {
            user = Meteor.user();
        }

        var config = Accounts.loginServiceConfiguration.findOne({service: "google"});
        if (! config)
            throw new Meteor.Error(500, "Google service not configured.");

        if (! user.services || ! user.services.google || ! user.services.google.refreshToken)
            throw new Meteor.Error(500, "Refresh token not found.");

        try {
            var result = Meteor.http.call("POST",
              "https://accounts.google.com/o/oauth2/token",
              {
                  params: {
                      'client_id': config.clientId,
                      'client_secret': config.secret,
                      'refresh_token': user.services.google.refreshToken,
                      'grant_type': 'refresh_token'
                  }
              });

            console.log("Result!", result);

        } catch (e) {
            console.log("Error", e);
            var code = e.response ? e.response.statusCode : 500;
            throw new Meteor.Error(code, 'Unable to exchange google refresh token.', e.response)
        }

        if (result.statusCode === 200) {
            // console.log('success');
            // console.log(EJSON.stringify(result.data));

            Meteor.users.update(user._id, {
                '$set': {
                    'services.google.accessToken': result.data.access_token,
                    'services.google.expiresAt': (+new Date) + (1000 * result.data.expires_in),
                }
            });

            return result.data;
        } else {
            throw new Meteor.Error(result.statusCode, 'Unable to exchange google refresh token.', result);
        }
    }
});