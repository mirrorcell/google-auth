var origLogout = Meteor.logout;

Meteor.logout = function(myCallback) {
  var user = Meteor.user(),
    self = this,
    callback = myCallback || function() {};

  console.log("Started logout");

  if(!user) return origLogout(callback);
  if(!Meteor.isCordova) return origLogout(callback);

  var services = user.services;

  if(!services) return origLogout(callback);

  console.log("Checking Cordova Meteor Login Methods for User", Meteor.user());
  console.log("plugin" + GPlusOauth.plugin + "googservices"  + Gugo.Utils.Auth.hasGoogleServices(user));
  if(GPlusOauth.plugin && GPUtils.hasGoogleServices(user)) {
    console.log("We have g plus login, lets logout");
    GPlusOauth.logout(function() {
        return origLogout(callback);
    });
  } else {
    return origLogout(callback);
  }
};