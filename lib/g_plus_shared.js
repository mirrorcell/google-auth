GPUtils = {
  hasGoogleServices : function(user) {
    return user && user.services && user.services.google;
  }
};
