if(!(Meteor.settings.google.clientId && Meteor.settings.google.clientSecret )) {
  throw new Meteor.Error("404", "Google Client ID or Client Secret Were not Found");
}

var google = {
  clientId: Meteor.settings.google.clientId,
  clientSecret: Meteor.settings.google.clientSecret
};

Meteor.startup(function() {

  Accounts.loginServiceConfiguration.remove({
    service: "google"
  });

  Accounts.loginServiceConfiguration.insert({
    service: "google",
    clientId: google.clientId,
    secret: google.clientSecret
  });

});


Accounts.onCreateUser(function(opts, user) {
  if(Gugo.Utils.Auth.hasFacebookServices(user)) {

  }

  if(Gugo.Utils.Auth.hasGoogleServices(user)) {
    if(user.services.google.oAuthToken && !user.services.google.refreshToken) {
      user.services.google.refreshToken = user.services.google.oAuthToken;
    }
  }

  if(opts && opts.profile) {
    user.profile = opts.profile
  }

  return user;
});